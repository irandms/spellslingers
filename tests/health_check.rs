//! tests/health_check.rs

use once_cell::sync::Lazy;
use spellslingers::configuration::{get_configuration, DatabaseSettings};
use spellslingers::startup::run;
use spellslingers::telemetry::{get_subscriber, init_subscriber};
use sqlx::{Connection, Executor, PgConnection, PgPool};
use std::net::TcpListener;
use std::iter::zip;
use uuid::Uuid;

pub struct TestApp {
    pub address: String,
    pub db_pool: PgPool,
}

static TRACING: Lazy<()> = Lazy::new(|| {
    let default_filter_level = "info".to_string();
    let subscriber_name = "test".to_string();
    if std::env::var("TEST_LOG").is_ok() {
        let subscriber = get_subscriber(subscriber_name, default_filter_level, std::io::stdout);
        init_subscriber(subscriber);
    } else {
        let subscriber = get_subscriber(subscriber_name, default_filter_level, std::io::sink);
        init_subscriber(subscriber);
    };
});

async fn spawn_app() -> TestApp {
    Lazy::force(&TRACING);

    let listener = TcpListener::bind("127.0.0.1:0").expect("Failed to bind to random port");
    let port = listener.local_addr().unwrap().port();
    let address = format!("http://127.0.0.1:{}", port);

    let mut configuration = get_configuration().expect("Failed to read configuration.");
    configuration.database.database_name = Uuid::new_v4().to_string();
    let connection_pool = configure_database(&configuration.database).await;

    let server = run(listener, connection_pool.clone()).expect("Failed to bind address");
    let _ = tokio::spawn(server);
    TestApp {
        address,
        db_pool: connection_pool,
    }
}

pub async fn configure_database(config: &DatabaseSettings) -> PgPool {
    // Create database
    let mut connection = PgConnection::connect_with(&config.without_db())
        .await
        .expect("Failed to connect to Postgres");
    connection
        .execute(&*format!(r#"CREATE DATABASE "{}";"#, config.database_name).as_str())
        .await
        .expect("Failed to create database.");

    // Migrate database
    let connection_pool = PgPool::connect_with(config.with_db())
        .await
        .expect("Failed too connect to Postgres");
    sqlx::migrate!("./migrations")
        .run(&connection_pool)
        .await
        .expect("Failed to migrate the database.");

    connection_pool
}

// `tokio::test` is the testing equivalent of `tokio::main`.
// It also spares you from having to specify the `#[test]` attribute.
//
// You can inspect what code gets generated using
// `cargo expand --test health_check` (<- name of the test file)
#[tokio::test]
async fn health_check_works() {
    // Arrange
    let app = spawn_app();
    let client = reqwest::Client::new();

    // Act
    let response = client
        .get(&format!("{}/health_check", app.await.address))
        .send()
        .await
        .expect("Failed to execute request.");

    // Assert
    assert!(response.status().is_success());
    assert_eq!(Some(0), response.content_length());
}

#[tokio::test]
async fn enter_match_data_returns_200_for_valid_form_data() {
    // Arrange
    let app = spawn_app().await;
    let client = reqwest::Client::new();
    let players = vec![
        "Spencer",
        "Nick",
        "Cameron",
        "Griffin",
    ];
    let decks = vec![
        "SpencerDeck",
        "NickDeck",
        "CameronDeck",
        "GriffinDeck",
    ];
    let test_cases = vec![
        // TODO(irandms): Fix this test case so that it passes
        /*
        (
            "turns=10&location=Online&player1_id=1&player1_deck=1&player2_id=2&player2_deck=2",
            "10-turn match with location Online, two players.",
        ),
        */
        (
            "turns=7&location=Online&player1_id=1&player1_deck=1&player1_placement=1&player2_id=2&player2_deck=2&player2_placement=2&player3_id=3&player3_deck=3&player3_placement=3&player4_id=4&player4_deck=4&player4_placement=4",
            "10-turn match with location Online, four players, all fields supplied.",
        ),
    ];
    // To enter a valid match, we must first add players and their decks
    for (player, deck) in zip(players, decks) {
        let response = client
            .post(&format!("{}/enter_player", &app.address))
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(format!("name={}", player))
            .send()
            .await
            .expect("Failed to add player.");
        assert_eq!(
            200,
            response.status().as_u16(),
            "The enter_player API succeeded with 200 OK with payload {}.",
            player,
        );
        let response = client
            .post(&format!("{}/enter_deck", &app.address))
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(format!("name={}", deck))
            .send()
            .await
            .expect("Failed to add deck.");
        assert_eq!(
            200,
            response.status().as_u16(),
            "The enter_deck API succeeded with 200 OK with payload {}.",
            deck,
        );
    }

    for (valid_body, case_message) in test_cases {
        // Act
        let response = client
            .post(&format!("{}/enter_match_data", &app.address))
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(valid_body)
            .send()
            .await
            .expect("Failed to execute request.");

        // Assert
        assert_eq!(
            200,
            response.status().as_u16(),
            "The enter_match_Data API succeeded with 200 OK with payload {}.",
            case_message,
        );

        let saved = sqlx::query!("SELECT turns FROM match_data",)
            .fetch_one(&app.db_pool)
            .await
            .expect("Failed to fetch saved match data.");

        assert_eq!(saved.turns, 7);
    }
}

#[tokio::test]
async fn enter_match_data_returns_400_when_data_is_missing() {
    // Arrange
    let app = spawn_app().await;
    let client = reqwest::Client::new();
    let test_cases = vec![
        ("location=Online", "missing everything but location"),
        (
            "turn_order=turn_order=playerid%3A1%2Cplayerid%3A2%2Cplayerid%3A3%2Cplayerid%3A4",
            "only includes turn order",
        ),
    ];

    for (invalid_body, error_message) in test_cases {
        // Act
        let response = client
            .post(&format!("{}/enter_match_data", &app.address))
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(invalid_body)
            .send()
            .await
            .expect("Failed to execute request.");

        // Assert
        assert_eq!(
            400,
            response.status().as_u16(),
            "The API did not fail with 400 Bad Request with payload {}.",
            error_message
        );
    }
}

#[tokio::test]
async fn enter_match_data_returns_a_400_when_description_is_present_but_empty() {
    // Arrange
    let app = spawn_app().await;
    let client = reqwest::Client::new();
    let test_cases = vec![("turns=5&description=", "empty description")];

    for (body, description) in test_cases {
        // Act
        let response = client
            .post(&format!("{}/enter_match_data", &app.address))
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(body)
            .send()
            .await
            .expect("Failed to execute request.");

        // Assert
        assert_eq!(
            400,
            response.status().as_u16(),
            "The API did not return a 400 Bad Request when the payload was {}.",
            description
        );
    }
}
