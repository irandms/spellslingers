//! src/routes/enter_deck.rs

use crate::domain::NewDeck;
use actix_web::{web, HttpResponse};
use sqlx::types::Uuid as SqlxUuid;
use sqlx::PgPool;
use uuid::Uuid;

#[derive(serde::Deserialize)]
pub struct FormData {
    name: String,
}

#[tracing::instrument(
    name = "Adding new deck",
    skip(form, pool),
    fields(
        name = %form.name,
    )
)]
pub async fn enter_deck(form: web::Form<FormData>, pool: web::Data<PgPool>) -> HttpResponse {
    let new_deck = NewDeck { name: form.0.name };

    tracing::info!("Saving new deck details in the database");
    match insert_deck(&new_deck, &pool).await {
        Ok(_) => HttpResponse::Ok().finish(),
        Err(_) => HttpResponse::InternalServerError().finish(),
    }
}

#[tracing::instrument(name = "Saving new deck details in the database", skip(new_deck, pool))]
pub async fn insert_deck(new_deck: &NewDeck, pool: &PgPool) -> Result<(), sqlx::Error> {
    sqlx::query!(
        r#"
        INSERT INTO decks (id, name)
        VALUES ($1, $2)
        "#,
        SqlxUuid::from_bytes(Uuid::new_v4().into_bytes()),
        new_deck.name,
    )
    .execute(pool)
    .await
    .map_err(|e| {
        tracing::error!("Failed to execute query: {:?}", e);
        e
    })?;
    Ok(())
}
