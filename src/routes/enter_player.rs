//! src/routes/enter_player.rs

use crate::domain::NewPlayer;
use actix_web::{web, HttpResponse};
use sqlx::types::Uuid as SqlxUuid;
use sqlx::PgPool;
use uuid::Uuid;

#[derive(serde::Deserialize)]
pub struct FormData {
    name: String,
}

#[tracing::instrument(
    name = "Adding new player",
    skip(form, pool),
    fields(
        name = %form.name,
    )
)]
pub async fn enter_player(form: web::Form<FormData>, pool: web::Data<PgPool>) -> HttpResponse {
    let new_player = NewPlayer { name: form.0.name };

    tracing::info!("Saving new player details in the database");
    match insert_player(&new_player, &pool).await {
        Ok(_) => HttpResponse::Ok().finish(),
        Err(_) => HttpResponse::InternalServerError().finish(),
    }
}

#[tracing::instrument(
    name = "Saving new player details in the database",
    skip(new_player, pool)
)]
pub async fn insert_player(new_player: &NewPlayer, pool: &PgPool) -> Result<(), sqlx::Error> {
    sqlx::query!(
        r#"
        INSERT INTO players (id, name)
        VALUES ($1, $2)
        "#,
        SqlxUuid::from_bytes(Uuid::new_v4().into_bytes()),
        new_player.name,
    )
    .execute(pool)
    .await
    .map_err(|e| {
        tracing::error!("Failed to execute query: {:?}", e);
        e
    })?;
    Ok(())
}
