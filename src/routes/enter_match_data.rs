//! src/routes/enter_match_data.rs

use crate::domain::{DeckID, Location, MatchData, Placement, PlayerID, Turns};
use actix_web::{web, HttpResponse};
use chrono::Utc;
use sqlx::types::Uuid as SqlxUuid;
use sqlx::PgPool;
use uuid::Uuid;

#[derive(serde::Deserialize)]
pub struct FormData {
    turns: i32,
    location: String,
    player1_id: i32,
    player1_deck: i32,
    player1_placement: i32,
    player2_id: i32,
    player2_deck: i32,
    player2_placement: i32,
    player3_id: i32,
    player3_deck: i32,
    player3_placement: i32,
    player4_id: i32,
    player4_deck: i32,
    player4_placement: i32,
}

#[tracing::instrument(
    name = "Adding new match data",
    skip(form, pool),
    fields(
        match_turns = %form.turns,
        match_location = %form.location,
    )
)]
pub async fn enter_match_data(form: web::Form<FormData>, pool: web::Data<PgPool>) -> HttpResponse {
    let player1_id = match PlayerID::parse(form.0.player1_id) {
        Ok(player1_id) => player1_id,
        Err(_) => return HttpResponse::BadRequest().finish(),
    };

    let player1_deck = match DeckID::parse(form.0.player1_deck) {
        Ok(player1_deck) => player1_deck,
        Err(_) => return HttpResponse::BadRequest().finish(),
    };

    let player2_id = match PlayerID::parse(form.0.player2_id) {
        Ok(player2_id) => player2_id,
        Err(_) => return HttpResponse::BadRequest().finish(),
    };

    let player2_deck = match DeckID::parse(form.0.player2_deck) {
        Ok(player2_deck) => player2_deck,
        Err(_) => return HttpResponse::BadRequest().finish(),
    };

    let match_data = MatchData {
        turns: Turns::parse(form.0.turns).unwrap_or_default(),
        location: Location::parse(form.0.location).unwrap_or_default(),
        player1_id,
        player1_deck,
        player1_placement: Some(Placement::try_from(form.0.player1_placement).unwrap_or_default()),
        player2_id,
        player2_deck,
        player2_placement: Some(Placement::try_from(form.0.player2_placement).unwrap_or_default()),
        player3_id: PlayerID::parse(form.0.player3_id).ok(),
        player3_deck: DeckID::parse(form.0.player3_deck).ok(),
        player3_placement: Some(Placement::try_from(form.0.player3_placement).unwrap_or_default()),
        player4_id: PlayerID::parse(form.0.player4_id).ok(),
        player4_deck: DeckID::parse(form.0.player4_deck).ok(),
        player4_placement: Some(Placement::try_from(form.0.player4_placement).unwrap_or_default()),
    };

    tracing::info!("Saving new match details in the database");
    match insert_match_data(&match_data, &pool).await {
        Ok(_) => HttpResponse::Ok().finish(),
        Err(_) => HttpResponse::InternalServerError().finish(),
    }
}

#[tracing::instrument(
    name = "Saving new match details in the database",
    skip(match_data, pool)
)]
pub async fn insert_match_data(
    match_data: &MatchData,
    pool: &PgPool,
) -> Result<(), sqlx::Error> {
    let now = Utc::now();
    let player1_placement = match_data.player1_placement.unwrap();
    let player2_placement = match_data.player2_placement.unwrap();
    let player3_placement = match_data.player3_placement;
    let player4_placement = match_data.player4_placement;

    let player3_id = match_data.player3_id;
    let player3_deck = match_data.player3_deck;
    let player4_id = match_data.player4_id;
    let player4_deck = match_data.player4_deck;
    sqlx::query!(
        r#"
        INSERT INTO match_data (id, turns, location, match_start_time, match_end_time, player1_id, player1_deck, player1_placement, player2_id, player2_deck, player2_placement, player3_id, player3_deck, player3_placement, player4_id, player4_deck, player4_placement)
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17)
        "#,
        SqlxUuid::from_bytes(Uuid::new_v4().into_bytes()),
        match_data.turns.as_ref(),
        match_data.location.as_ref(),
        now,
        now,
        match_data.player1_id.as_ref(),
        match_data.player1_deck.as_ref(),
        player1_placement as Placement,
        match_data.player2_id.as_ref(),
        match_data.player2_deck.as_ref(),
        player2_placement as Placement,
        player3_id as Option<PlayerID>,
        player3_deck as Option<DeckID>,
        player3_placement as Option<Placement>,
        player4_id as Option<PlayerID>,
        player4_deck as Option<DeckID>,
        player4_placement as Option<Placement>,
    )
    .execute(pool)
    .await
    .map_err(|e| {
        tracing::error!("Failed to execute query: {:?}", e);
        e
    })?;
    Ok(())
}
