use serde::{Serialize, Deserialize};
use sqlx::Type;

#[derive(Type, Serialize, Deserialize)]
#[sqlx(transparent)]
pub struct Turns(i32);

impl Default for Turns {
    fn default() -> Self {
        Self(1)
    }
}

impl Turns {
    pub fn parse(i: i32) -> Result<Turns, String> {
        let is_negative = i.is_negative();

        if is_negative {
            Err(format!("{} is negative; turns should be positive.", i))
        } else {
            Ok(Self(i))
        }
    }
}

impl AsRef<i32> for Turns {
    fn as_ref(&self) -> &i32 {
        &self.0
    }
}
