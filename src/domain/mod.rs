//! src/domain/mod.rs

mod deck;
mod location;
mod new_deck;
mod new_match_data;
mod new_player;
mod placement;
mod player;
mod time;
mod turns;

pub use deck::DeckID;
pub use location::Location;
pub use new_deck::NewDeck;
pub use new_match_data::MatchData;
pub use new_player::NewPlayer;
pub use placement::Placement;
pub use player::PlayerID;
pub use turns::Turns;
