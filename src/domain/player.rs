//! src/domain/player.rs

use serde::{Serialize, Deserialize};
use sqlx::Type;

#[derive(Clone, Copy, Serialize, Deserialize, Type)]
#[sqlx(transparent)]
pub struct PlayerID(i32);

impl PlayerID {
    pub fn parse(i: i32) -> Result<PlayerID, String> {
        let is_positive = i.is_positive();

        if !is_positive {
            Err(format!("{} is not positive; IDs start from 1.", i))
        } else {
            Ok(Self(i))
        }
    }
}

impl AsRef<i32> for PlayerID {
    fn as_ref(&self) -> &i32 {
        &self.0
    }
}
