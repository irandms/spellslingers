//! src/domain/deck.rs

use serde::{Serialize, Deserialize};
use sqlx::Type;

#[derive(Clone, Copy, Serialize, Deserialize, Type)]
#[sqlx(transparent)]
pub struct DeckID(i32);

impl DeckID {
    pub fn parse(i: i32) -> Result<DeckID, String> {
        let is_positive = i.is_positive();

        if !is_positive {
            Err(format!("{} is not positive; IDs start from 1.", i))
        } else {
            Ok(Self(i))
        }
    }
}

impl AsRef<i32> for DeckID {
    fn as_ref(&self) -> &i32 {
        &self.0
    }
}
