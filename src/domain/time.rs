use chrono::prelude::*;
use sqlx::Type;

#[derive(Type)]
pub struct StartTime(DateTime<Utc>);
