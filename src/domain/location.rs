//! src/domain/location.rs

use serde::{Serialize, Deserialize};
use sqlx::Type;
use unicode_segmentation::UnicodeSegmentation;

#[derive(Clone, Debug, Serialize, Deserialize, Type)]
#[sqlx(transparent)]
pub struct Location(String);

impl Location {
    pub fn parse(s: String) -> Result<Location, String> {
        let is_empty_or_whitespace = s.trim().is_empty();
        let is_too_long = s.graphemes(true).count() > 64;
        let forbidden_characters = ['/', '(', ')', '"', '<', '>', '\\', '{', '}'];
        let contains_forbidden_characters = s.chars().any(|g| forbidden_characters.contains(&g));

        if is_empty_or_whitespace || is_too_long || contains_forbidden_characters {
            Err(format!("{} is not a valid location.", s))
        } else {
            Ok(Self(s))
        }
    }
}

impl AsRef<str> for Location {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl Default for Location {
    fn default() -> Self {
        Location::parse("Unknown".to_string()).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use crate::domain::Location;
    use claim::{assert_err, assert_ok};

    #[test]
    fn a_64_grapheme_long_location_is_valid() {
        let location = "ё".repeat(64);
        assert_ok!(Location::parse(location));
    }

    #[test]
    fn a_location_longer_than_65_graphemes_is_rejected() {
        let location = "a".repeat(65);
        assert_err!(Location::parse(location));
    }

    #[test]
    fn whitespace_only_locations_are_rejected() {
        let location = " ".to_string();
        assert_err!(Location::parse(location));
    }

    #[test]
    fn empty_string_is_rejected() {
        let location = "".to_string();
        assert_err!(Location::parse(location));
    }

    #[test]
    fn locations_containing_an_invalid_character_are_rejected() {
        for location in &['/', '(', ')', '"', '<', '>', '\\', '{', '}'] {
            let location = location.to_string();
            assert_err!(Location::parse(location));
        }
    }

    #[test]
    fn a_valid_location_is_parsed_successfully() {
        let location = "Online".to_string();
        assert_ok!(Location::parse(location));
    }
}
