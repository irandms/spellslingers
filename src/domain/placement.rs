//! src/domain/placement.rs

use serde::{Serialize, Deserialize};
use sqlx::Type;

#[derive(Clone, Copy, Serialize, Deserialize, Type)]
#[sqlx(type_name = "placement", rename_all = "lowercase")]
pub enum Placement {
    First = 1,
    Second,
    Third,
    Fourth,
}

impl Default for Placement {
    fn default() -> Self {
        Placement::Fourth
    }
}

impl TryFrom<i32> for Placement {
    type Error = String;

    fn try_from(v: i32) -> Result<Placement, Self::Error> {
        match v {
            x if x == Placement::First as i32 => Ok(Placement::First),
            x if x == Placement::Second as i32 => Ok(Placement::Second),
            x if x == Placement::Third as i32 => Ok(Placement::Third),
            x if x == Placement::Fourth as i32 => Ok(Placement::Fourth),
            _ => Err("Invalid Placement provided.".to_string()),
        }
    }
}
