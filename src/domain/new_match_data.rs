use crate::domain::{Turns, Location, PlayerID, DeckID, Placement};
use serde::Serialize;

#[derive(Serialize)]
pub struct MatchData {
    pub turns: Turns,
    pub location: Location,
    pub player1_id: PlayerID,
    pub player1_deck: DeckID,
    pub player1_placement: Option<Placement>,
    pub player2_id: PlayerID,
    pub player2_deck: DeckID,
    pub player2_placement: Option<Placement>,
    pub player3_id: Option<PlayerID>,
    pub player3_deck: Option<DeckID>,
    pub player3_placement: Option<Placement>,
    pub player4_id: Option<PlayerID>,
    pub player4_deck: Option<DeckID>,
    pub player4_placement: Option<Placement>,
}
