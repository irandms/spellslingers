-- Add migration script here
CREATE TABLE decks (
    pk INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    id UUID NOT NULL,
    name TEXT NOT NULL
);
