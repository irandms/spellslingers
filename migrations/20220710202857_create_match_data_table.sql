CREATE TYPE placement AS ENUM ('first', 'second', 'third', 'fourth');

-- Create Match Data table
CREATE TABLE match_data (
    pk INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    id UUID NOT NULL,
    turns INT NOT NULL,
    location TEXT NOT NULL,
    match_start_time TIMESTAMPTZ NOT NULL,
    match_end_time TIMESTAMPTZ NOT NULL,
    player1_id INT REFERENCES players NOT NULL,
    player1_deck INT REFERENCES decks NOT NULL,
    player1_placement placement,
    player2_id INT REFERENCES players NOT NULL,
    player2_deck INT REFERENCES decks NOT NULL,
    player2_placement placement,
    player3_id INT REFERENCES players,
    player3_deck INT REFERENCES decks,
    player3_placement placement,
    player4_id INT REFERENCES players,
    player4_deck INT REFERENCES decks,
    player4_placement placement
);
