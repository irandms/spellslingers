-- Adding initial Players table
CREATE TABLE players (
    pk INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    id UUID NOT NULL,
    name TEXT NOT NULL
);
